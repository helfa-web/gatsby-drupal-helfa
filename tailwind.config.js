const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: [
    "./static/**/*.{html,js}",
    "./src/templates/**/*.{html,js}",
    "./src/pages/**/*.{html,js}",
    "./src/components/**/*.{html,js}",
  ],
  // Values which are sometimes calculated, so that they don't necessarily
  // appear as complete strings need to be whitelisted.
  safelist: ["bg-helfa-light-green", "bg-helfa-beige"],
  theme: {
    container: {
      center: true,
    },
    extend: {
      fontFamily: {
        sans: ['"Fira Sans Condensed"', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        "helfa-light-green": "#8bbb74",
        "helfa-dark-green": "#3e8e18",
        "helfa-darker-green": "#2a6110",
        "helfa-beige": "#fffaee",
        "helfa-beige-darker": "#ece3d0",
        "helfa-light": "#ffffff",
        "helfa-brown": "#391910",
        "helfa-gray": "#323030",
      },
    },
  },
  variants: {},
  plugins: [],
};
