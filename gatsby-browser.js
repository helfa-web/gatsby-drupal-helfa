// import "@fontsource/fira-sans-condensed/100.css"; // thin
// import "@fontsource/fira-sans-condensed/200.css"; // extra light
import "@fontsource/fira-sans-condensed/300.css"; // light
import "@fontsource/fira-sans-condensed/400.css"; // normal
import "@fontsource/fira-sans-condensed/400-italic.css"; // italics
import "@fontsource/fira-sans-condensed/500.css"; // medium
import "@fontsource/fira-sans-condensed/600.css"; // semibold
import "@fontsource/fira-sans-condensed/700.css"; // bold
// import "@fontsource/fira-sans-condensed/800.css"; // extrabold
// import "@fontsource/fira-sans-condensed/900.css"; // black

import "./src/styles/global.css";
