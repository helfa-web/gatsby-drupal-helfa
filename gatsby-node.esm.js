import path from "path";
import { groups } from "./src/mock/local-groups";

export const onCreatePage = ({ page }) => {
  console.log("on create page: " + page.path);
};

export const createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const markdownPageTemplate = path.resolve(`src/templates/markdown-page.js`);

  const result = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            id
            frontmatter {
              title
              slug
            }
          }
        }
      }
    }
  `);

  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      component: markdownPageTemplate,
      path: `${node.frontmatter.slug}`,
      context: {
        id: node.id,
        title: node.frontmatter.title,
      },
    });
  });

  groups.forEach(group => {
    createPage({
      component: path.resolve(`src/templates/group-page.js`),
      path: `/local-groups/${group.id}`,
      context: {
        id: group.id,
        group,
      },
    });
  });
};
