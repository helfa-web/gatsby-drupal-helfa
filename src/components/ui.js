import React, { useState } from "react";
import clsx from "clsx";
import ReactPlayer from "react-player";
import { Link, useTranslation } from "gatsby-plugin-react-i18next";

import playIcon from "../images/playBtn.svg";
import ChevronDownIcon from "../images/chevron-down.inline.svg";

export const VideoContainer = () => {
  const { t } = useTranslation("videocontainer");
  return (
    <div className="w-[90%] mx-auto mb-4 relative pt-[56%]">
      <ReactPlayer
        url="https://www.youtube.com/watch?v=P3DboxBkZEY"
        light={true}
        playIcon={<img src={playIcon} alt={t("Wiedergabe Icon")} />}
        width="100%"
        height="100%"
        className="absolute top-0 left-0"
      />
    </div>
  );
};

export const Section = ({
  backgroundColor,
  color,
  children,
  className,
  tag,
  ...props
}) => {
  const TagType = tag;
  const bgColor = backgroundColor ? `bg-${backgroundColor}` : "";
  const textColor = color ? `text-${color}` : "";
  return (
    <TagType className={clsx(bgColor, textColor, className)} {...props}>
      <div className="content mx-auto">
        <div className="p-6 text-center">{children}</div>
      </div>
    </TagType>
  );
};

Section.defaultProps = {
  tag: "section",
};

export const Footer = props => {
  return Section({ tag: "footer", ...props });
};

export const Button = ({ to, children, disabled, className }) => {
  const options = { to, disabled };

  return (
    <Link
      {...options}
      className={clsx(
        className,
        "rounded-full bg-helfa-light-green py-3 m-1 inline-block font-medium"
      )}
    >
      {children}
    </Link>
  );
};

export const Accordion = ({ children }) => {
  const expandedIndex = children.findIndex(element => element.props.expanded);

  const [expanded, setExpanded] = useState(expandedIndex);

  const clickHandler = index => () => {
    if (expanded === index) {
      setExpanded(null);
      return;
    }
    setExpanded(index);
  };

  let i = 0;

  const childrenWithProps = React.Children.map(children, child => {
    const clone = React.cloneElement(child, {
      onClick: clickHandler(i),
      expanded: i === expanded,
    });
    i++;
    return clone;
  });

  return <div>{childrenWithProps}</div>;
};

export const Expandable = ({ children, expanded, title, onClick }) => {
  return (
    <div className="text-left mb-2">
      <div
        onClick={onClick}
        className="text-xl p-3 text-left rounded flex justify-between items-center w-full bg-gray-200"
      >
        <h3 className="inline-block">{title}</h3>
        <ChevronDownIcon
          stroke="currentColor"
          className={clsx("duration-100 transition-transform ease-in", {
            "rotate-180": expanded,
          })}
        />
      </div>
      <div className={clsx("text-left p-1", { hidden: !expanded })}>
        {children}
      </div>
    </div>
  );
};

export const Collapsible = ({ children, initiallyCollapsed, title }) => {
  const [collapsed, setCollapsed] = useState(initiallyCollapsed);
  const handleClick = () => {
    setCollapsed(!collapsed);
  };
  return (
    <div className="text-left mb-2">
      <div
        onClick={handleClick}
        className={clsx(
          "text-md font-semibold p-3 text-left rounded-t flex justify-between items-center w-full bg-helfa-light-green",
          { "rounded-b": collapsed }
        )}
      >
        <h3 className="inline-block">{title}</h3>
        <ChevronDownIcon
          stroke="currentColor"
          className={clsx("duration-100 transition-transform ease-in", {
            "rotate-180": !collapsed,
          })}
        />
      </div>
      <div
        className={clsx("text-left p-2 bg-helfa-light-green rounded-b", {
          hidden: collapsed,
        })}
      >
        {children}
      </div>
    </div>
  );
};

Collapsible.defaultProps = {
  initiallyCollapsed: true,
};
