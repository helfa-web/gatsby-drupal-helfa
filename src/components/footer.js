import React from "react";
import { Link, useTranslation } from "gatsby-plugin-react-i18next";

import { Footer } from "./ui";
import LanguageSwitcher from "./language-switcher";

import TelegramIcon from "../images/Telegram.inline.svg";
import YouTubeIcon from "../images/YouTube.inline.svg";

const HelfaFooter = () => {
  const { t } = useTranslation("footer");
  return (
    <Footer backgroundColor="helfa-light-green">
      <div className="text-white flex justify-between flex-col md:flex-row">
        <div className="text-left mb-3">
          <LanguageSwitcher />
          <div>
            <ul className="list-none">
              <Link to="/contact">
                <li className="inline-block p-1">{t("Kontakt")}</li>
              </Link>
              <Link to="/imprint">
                <li className="inline-block p-1 before:content-['|'] before:mr-2">
                  {t("Impressum")}
                </li>
              </Link>
              <Link to="/privacy">
                <li className="inline-block p-1 before:content-['|'] before:mr-2">
                  {t("Datenschutz")}
                </li>
              </Link>
            </ul>
          </div>
        </div>
        <div className="text-left md:text-right">
          <div className="py-2">
            <a href="https://t.me/Helfa_Projekt">
              <TelegramIcon className="w-8 mr-3 inline-block" />
            </a>
            <a href="https://www.youtube.com/channel/UCt0yWUqrpIjshR3rbtEpDYg">
              <YouTubeIcon className="w-8 inline-block" />
            </a>
          </div>
          <div>&copy; H.e.l.f.a.- {new Date().getFullYear()}</div>
        </div>
      </div>
    </Footer>
  );
};

export default HelfaFooter;
