// Shim to write config in es6, because there's shared data between
// config and app.
const requireEsm = require("esm")(module);
module.exports = requireEsm("./gatsby-config.esm.js");
